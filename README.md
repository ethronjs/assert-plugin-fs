# @ethronjs/assert.plugin.fs

[![NPM version](https://img.shields.io/npm/v/@ethronjs/assert.plugin.fs.svg)](https://npmjs.org/package/@ethronjs/assert.plugin.fs)
[![Total downloads](https://img.shields.io/npm/dt/@ethronjs/assert.plugin.fs.svg)](https://npmjs.org/package/@ethronjs/assert.plugin.fs)

Test assertion plugin for files and directories.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*

## Use

```
const assert = require("@ethronjs/assert");
require("@ethronjs/assert.plugin.fs");
```

When the plugin is imported, this is plugged in automatically.

## File assertions

To work with files, we have to use the function `assert.file()`:

```
assert.file(...path)
```

Next we can use assertion methods such as:

```
assert.file("/my/file.txt").exists()
assert.file("/my", "file.txt").doesNotExist()
```

### exists(), notExists() and doesNotExist()

With `exists()`, `notExists()` and `doesNotExist()` we check whether a file exists:

```
assert.file(...path).exists()
assert.file(...path).notExists()
assert.file(...path).doesNotExist()
```

### isEmpty() and isNotEmpty()

With `isEmpty()` and `isNotEmpty()` we check whether a file is empty:

```
assert.file(path).isEmpty()
assert.file(path).isNotEmpty()
```

### includes(), notIncludes() and doesNotInclude()

With `includes()`, `notIncludes()` and `doesNotInclude()` we check whether a file contains a given content:

```
assert.file(path).includes(txt)
assert.file(path).notIncludes(txt)
assert.file(path).doesNotInclude(txt)
```

### eq(), ne() and neq()

With `eq()` and `ne()` we check whether the file content is given one:

```
assert.file(path).eq(txt)
assert.file(path).ne(txt)
```

`ne()` has the alias `neq()`.

### isJson()

With `isJson()` we can check if a file has a JSON format:

```
assert.file(path).isJson()
```

### sameAs() and notSameAs()

With `sameAs()` and `notSameAs()` we check whether two files contains the same:

```
assert.file(path1).sameAs(path2)
assert.file(path1).notSameAs(path2)
```

### startsWith(), notStartsWith() and doesNotStartWith()

With `startsWith()`, we check if the file starts with a text:

```
assert.file(path).startsWith(txt)
assert.file(path).notStartsWith(txt)
assert.file(path).doesNotStartWith(txt)
```

### endsWith(), notEndsWith() and doesNotEndWith()

With `endsWith()`, we check if the file ends with a text:

```
assert.file(path).endsWith(txt)
assert.file(path).notEndsWith(txt)
assert.file(path).doesNotEndWith(txt)
```

## Directory assertions

To work with directories, we have to use the function `assert.dir()`:

```
assert.dir(...args)
```

Next we can use assertion methods such as:

```
assert.dir("/my/dir").exists()
assert.dir("/my", "dir").doesNotExist()
```

### exists(), notExists() and doesNotExist()

With `exists()`, `notExists()` and `doesNotExist()` we check whether a directory exists:

```
assert.dir(path).exists()
assert.dir(path).notExists()
assert.dir(path).doesNotExist()
```

### has(), notHas() and doesNotHave()

With `has()`, `notHas()` and `doesNotHave()` we check whether a directory has a child entry (such as a file or a dir):

```
assert.dir(path).has(name)
assert.dir(path).notHas(name)
assert.dir(path).doesNotHave(name)
```
