"use strict";

var _core = require("@dogmalang/core");

const path = _core.dogma.use(require("@dogmalang/path"));

const fs = _core.dogma.use(require("@dogmalang/fs.sync"));

const $DirWrapper = class DirWrapper {
  constructor(...args) {
    {
      if ((0, _core.len)(args) == 0) {
        _core.dogma.raise("dir expected.");
      }

      _core.dogma.update(this, {
        name: "path",
        visib: ":",
        assign: "::=",
        value: path.join(...args)
      });
    }
  }

};
const DirWrapper = new Proxy($DirWrapper, {
  apply(receiver, self, args) {
    return new $DirWrapper(...args);
  }

});
module.exports = exports = DirWrapper;
const Self = DirWrapper;

Self.prototype.exists = function () {
  {
    if (!fs.isDir(this._path)) {
      _core.dogma.raise("dir '%s' must exist.", this._path);
    }
  }
  return this;
};

Self.prototype.doesNotExist = Self.prototype.notExists = function () {
  {
    if (fs.isDir(this._path)) {
      _core.dogma.raise("dir '%s' must not exist.", this._path);
    }
  }
  return this;
};

Self.prototype.has = function (entries) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("entries", entries, [_core.list, _core.text]);

  entries = (0, _core.list)(entries);
  {
    this.exists();

    for (const e of entries) {
      if (!fs.exists(path.join(this._path, e))) {
        _core.dogma.raise("dir '%s' must have entry '%s'.", this._path, e);
      }
    }
  }
  return this;
};

Self.prototype.doesNotHave = Self.prototype.notHas = function (entries) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("entries", entries, [_core.list, _core.text]);

  entries = (0, _core.list)(entries);
  {
    this.exists();

    for (const e of entries) {
      if (fs.exists(path.join(this._path, e))) {
        _core.dogma.raise("dir '%s' must not have entry '%s'.", this._path, e);
      }
    }
  }
  return this;
};