"use strict";

var _core = require("@dogmalang/core");

const DirWrapper = _core.dogma.use(require("./DirWrapper"));

const FileWrapper = _core.dogma.use(require("./FileWrapper"));

const assert = _core.dogma.use(require("@ethronjs/assert"));

const api = {
  ["dir"]: (...args) => {
    {
      return DirWrapper(...args);
    }
  },
  ["file"]: (...args) => {
    {
      return FileWrapper(...args);
    }
  }
};
module.exports = exports = api;
assert.plugin(api);