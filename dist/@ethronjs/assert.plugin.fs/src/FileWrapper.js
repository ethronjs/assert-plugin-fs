"use strict";

var _core = require("@dogmalang/core");

const path = _core.dogma.use(require("@dogmalang/path"));

const fs = _core.dogma.use(require("@dogmalang/fs.sync"));

const $FileWrapper = class FileWrapper {
  constructor(...args) {
    {
      if ((0, _core.len)(args) == 0) {
        _core.dogma.raise("file expected.");
      }

      _core.dogma.update(this, {
        name: "path",
        visib: ":",
        assign: "::=",
        value: path.join(...args)
      });
    }
  }

};
const FileWrapper = new Proxy($FileWrapper, {
  apply(receiver, self, args) {
    return new $FileWrapper(...args);
  }

});
module.exports = exports = FileWrapper;
const Self = FileWrapper;

Self.prototype.exists = function () {
  {
    if (!fs.isFile(this._path)) {
      _core.dogma.raise("file '%s' must exist.", this._path);
    }
  }
  return this;
};

Self.prototype.doesNotExist = Self.prototype.notExists = function () {
  {
    if (fs.isFile(this._path)) {
      _core.dogma.raise("file '%s' must not exist.", this._path);
    }
  }
  return this;
};

Self.prototype.includes = function (texts) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("texts", texts, [_core.list, _core.any]);

  texts = (0, _core.list)(texts);
  {
    let cont;
    this.exists();
    cont = fs.file(this._path).read();

    for (const t of texts) {
      if (!cont.includes((0, _core.text)(t))) {
        _core.dogma.raise("file '%s' must include '%s'.", this._path, t);
      }
    }
  }
  return this;
};

Self.prototype.doesNotInclude = Self.prototype.notIncludes = function (texts) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("texts", texts, [_core.list, _core.any]);

  texts = (0, _core.list)(texts);
  {
    let cont;
    this.exists();
    cont = fs.file(this._path).read();

    for (const t of texts) {
      if (cont.includes((0, _core.text)(t))) {
        _core.dogma.raise("file '%s' must not include '%s'.", this._path, t);
      }
    }
  }
  return this;
};

Self.prototype.isEmpty = function () {
  {
    this.exists();

    if (fs.file(this._path).len() > 0) {
      _core.dogma.raise("file '%s' must be empty.", this._path);
    }
  }
  return this;
};

Self.prototype.isNotEmpty = function () {
  {
    this.exists();

    if (fs.file(this._path).len() == 0) {
      _core.dogma.raise("file '%s' must not be empty.", this._path);
    }
  }
  return this;
};

Self.prototype.isJson = function () {
  {
    this.exists();
    {
      const [ok, err] = _core.dogma.peval(() => {
        return _core.json.decode(fs.file(this._path).read("utf-8"));
      });

      if (!ok) {
        _core.dogma.raise("file '%s' must be JSON.", this._path);
      }
    }
  }
  return this;
};

Self.prototype.eq = function (cont) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("cont", cont, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf8") != cont) {
      _core.dogma.raise("'%s' file content must be '%s'.", this._path, cont);
    }
  }
  return this;
};

Self.prototype.neq = Self.prototype.ne = function (cont) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("cont", cont, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf8") == cont) {
      _core.dogma.raise("'%s' file content must be '%s'.", this._path, cont);
    }
  }
  return this;
};

Self.prototype.sameAs = function (path) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("path", path, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf8") != fs.file(path).read("utf8")) {
      _core.dogma.raise("file '%s' must be same as file '%s'.", this._path, path);
    }
  }
  return this;
};

Self.prototype.notSameAs = function (path) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("path", path, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf8") == fs.file(path).read("utf8")) {
      _core.dogma.raise("file '%s' must not be same as file '%s'.", this._path, path);
    }
  }
  return this;
};

Self.prototype.startsWith = function (prefix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("prefix", prefix, _core.text);

  {
    this.exists();

    if (!fs.file(this._path).read("utf-8").startsWith(prefix)) {
      _core.dogma.raise("file '%s' must start with '%s'.", this._path, prefix);
    }
  }
  return this;
};

Self.prototype.doesNotStartWith = Self.prototype.notStartsWith = function (prefix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("prefix", prefix, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf-8").startsWith(prefix)) {
      _core.dogma.raise("file '%s' must not start with '%s'.", this._path, prefix);
    }
  }
  return this;
};

Self.prototype.endsWith = function (suffix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("suffix", suffix, _core.text);

  {
    this.exists();

    if (!fs.file(this._path).read("utf-8").endsWith(suffix)) {
      _core.dogma.raise("file '%s' must end with '%s'.", this._path, suffix);
    }
  }
  return this;
};

Self.prototype.doesNotEndWith = Self.prototype.notEndsWith = function (suffix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("suffix", suffix, _core.text);

  {
    this.exists();

    if (fs.file(this._path).read("utf-8").endsWith(suffix)) {
      _core.dogma.raise("file '%s' must not end with '%s'.", this._path, suffix);
    }
  }
  return this;
};