"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

_core.dogma.use(require("../../../../@ethronjs/assert.plugin.fs"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("dir()", () => {
      {
        (0, _ethron.test)("dir() - error when dir expected", () => {
          {
            assert(() => {
              {
                assert.dir();
              }
            }).raises("dir expected.");
          }
        });
        (0, _ethron.test)("dir(dir)", () => {
          {
            assert(assert.dir("test/data")).is("DirWrapper").mem("_path").eq("test/data");
          }
        });
        (0, _ethron.test)("dir(parent, name)", () => {
          {
            assert(assert.dir("test", "data")).isNotNil().mem("_path").eq("test/data");
          }
        });
      }
    });
    (0, _ethron.suite)("exists()", () => {
      {
        (0, _ethron.test)("exists()", () => {
          {
            const dir = assert.dir("test", "data");
            assert(dir.exists()).sameAs(dir);
          }
        });
        (0, _ethron.test)("exists() - error when not existing", () => {
          {
            assert(() => {
              {
                assert.dir("test", "unknown").exists();
              }
            }).raises("dir 'test/unknown' must exist.");
          }
        });
        (0, _ethron.test)("exists() - error when dir is file", () => {
          {
            assert(() => {
              {
                assert.dir("test", "data", "file.txt").exists();
              }
            }).raises("dir 'test/data/file.txt' must exist.");
          }
        });
      }
    });
    (0, _ethron.suite)("notExists()", () => {
      {
        (0, _ethron.test)("doesNotExist()", () => {
          {
            const dir = assert.dir("test", "unknown");
            assert(dir.doesNotExist()).sameAs(dir);
          }
        });
        (0, _ethron.test)("notExists()", () => {
          {
            const dir = assert.dir("test", "unknown");
            assert(dir.notExists()).sameAs(dir);
          }
        });
        (0, _ethron.test)("notExists() - when dir is file", () => {
          {
            const dir = assert.dir("test", "data", "file.txt");
            assert(dir.notExists()).sameAs(dir);
          }
        });
        (0, _ethron.test)("notExists() - error when existing", () => {
          {
            assert(() => {
              {
                assert.dir("test", "data").notExists();
              }
            }).raises("dir 'test/data' must not exist.");
          }
        });
      }
    });
    (0, _ethron.suite)("has()", () => {
      {
        (0, _ethron.test)("has(entry:text) - when file", () => {
          {
            const dir = assert.dir("test", "data");
            assert(dir.has("empty.txt")).sameAs(dir);
          }
        });
        (0, _ethron.test)("has(entry:text) - when dir", () => {
          {
            const dir = assert.dir("test");
            assert(dir.has("data")).sameAs(dir);
          }
        });
        (0, _ethron.test)("has(entry:text) - error", () => {
          {
            assert(() => {
              {
                assert.dir("test").has("unknown.txt");
              }
            }).raises("dir 'test' must have entry 'unknown.txt'.");
          }
        });
        (0, _ethron.test)("has(entry:list)", () => {
          {
            const dir = assert.dir("test", "data");
            assert(dir.has(["empty.txt"])).sameAs(dir);
          }
        });
      }
    });
    (0, _ethron.suite)("notHas()", () => {
      {
        (0, _ethron.test)("doesNotHave(entry)", () => {
          {
            const dir = assert.dir("test", "data");
            assert(dir.doesNotHave("unknown.txt")).sameAs(dir);
          }
        });
        (0, _ethron.test)("notHas(entry:list)", () => {
          {
            const dir = assert.dir("test", "data");
            assert(dir.notHas(["unknown.txt"])).sameAs(dir);
          }
        });
        (0, _ethron.test)("notHas(entry:text) - error when entry existing", () => {
          {
            assert(() => {
              {
                assert.dir("test").notHas("data");
              }
            }).raises("dir 'test' must not have entry 'data'.");
          }
        });
        (0, _ethron.test)("notHas(entry:text) - error when file instead of dir", () => {
          {
            assert(() => {
              {
                assert.dir("test/data/empty.txt").notHas("empty.txt");
              }
            }).raises("dir 'test/data/empty.txt' must exist.");
          }
        });
        (0, _ethron.test)("notHas(entry:text) - error when non-existing dir", () => {
          {
            assert(() => {
              {
                assert.dir("test/unknown").notHas("empty.txt");
              }
            }).raises("dir 'test/unknown' must exist.");
          }
        });
      }
    });
  }
});