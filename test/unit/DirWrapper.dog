#imports
from "ethron" use suite, test
use <assert>
use("../../../../@ethronjs/assert.plugin.fs")

#Suite.
export suite(__filename, proc()
  #########
  # dir() #
  #########
  suite("dir()", proc()
    test("dir() - error when dir expected", proc()
      assert(proc() assert.dir() end).raises("dir expected.")
    end)

    test("dir(dir)", proc()
      assert(assert.dir("test/data")).is("DirWrapper").mem("_path").eq("test/data")
    end)

    test("dir(parent, name)", proc()
      assert(assert.dir("test", "data")).isNotNil().mem("_path").eq("test/data")
    end)
  end)

  ############
  # exists() #
  ############
  suite("exists()", proc()
    test("exists()", proc()
      const dir = assert.dir("test", "data")
      assert(dir.exists()).sameAs(dir)
    end)

    test("exists() - error when not existing", proc()
      assert(proc()
        assert.dir("test", "unknown").exists()
      end).raises("dir 'test/unknown' must exist.")
    end)

    test("exists() - error when dir is file", proc()
      assert(proc()
        assert.dir("test", "data", "file.txt").exists()
      end).raises("dir 'test/data/file.txt' must exist.")
    end)
  end)

  ###############
  # notExists() #
  ###############
  suite("notExists()", proc()
    test("doesNotExist()", proc()
      const dir = assert.dir("test", "unknown")
      assert(dir.doesNotExist()).sameAs(dir)
    end)

    test("notExists()", proc()
      const dir = assert.dir("test", "unknown")
      assert(dir.notExists()).sameAs(dir)
    end)

    test("notExists() - when dir is file", proc()
      const dir = assert.dir("test", "data", "file.txt")
      assert(dir.notExists()).sameAs(dir)
    end)

    test("notExists() - error when existing", proc()
      assert(proc()
        assert.dir("test", "data").notExists()
      end).raises("dir 'test/data' must not exist.")
    end)
  end)

  #########
  # has() #
  #########
  suite("has()", proc()
    test("has(entry:text) - when file", proc()
      const dir = assert.dir("test", "data")
      assert(dir.has("empty.txt")).sameAs(dir)
    end)

    test("has(entry:text) - when dir", proc()
      const dir = assert.dir("test")
      assert(dir.has("data")).sameAs(dir)
    end)

    test("has(entry:text) - error", proc()
      assert(proc()
        assert.dir("test").has("unknown.txt")
      end).raises("dir 'test' must have entry 'unknown.txt'.")
    end)

    test("has(entry:list)", proc()
      const dir = assert.dir("test", "data")
      assert(dir.has(["empty.txt"])).sameAs(dir)
    end)
  end)

  ############
  # notHas() #
  ############
  suite("notHas()", proc()
    test("doesNotHave(entry)", proc()
      const dir = assert.dir("test", "data")
      assert(dir.doesNotHave("unknown.txt")).sameAs(dir)
    end)

    test("notHas(entry:list)", proc()
      const dir = assert.dir("test", "data")
      assert(dir.notHas(["unknown.txt"])).sameAs(dir)
    end)

    test("notHas(entry:text) - error when entry existing", proc()
      assert(proc()
        assert.dir("test").notHas("data")
      end).raises("dir 'test' must not have entry 'data'.")
    end)

    test("notHas(entry:text) - error when file instead of dir", proc()
      assert(proc()
        assert.dir("test/data/empty.txt").notHas("empty.txt")
      end).raises("dir 'test/data/empty.txt' must exist.")
    end)

    test("notHas(entry:text) - error when non-existing dir", proc()
      assert(proc()
        assert.dir("test/unknown").notHas("empty.txt")
      end).raises("dir 'test/unknown' must exist.")
    end)
  end)
end)
