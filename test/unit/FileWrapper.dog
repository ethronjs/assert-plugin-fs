#imports
from "ethron" use suite, test
use <assert>
use("../../../../@ethronjs/assert.plugin.fs")

#Suite.
export suite(__filename, proc()
  ##########
  # file() #
  ##########
  suite("file()", proc()
    test("file() - error", proc()
      assert(proc() assert.file() end).raises("file expected.")
    end)

    test("file(file)", proc()
      assert(assert.file("myfile")).is("FileWrapper").mem("_path").eq("myfile")
    end)

    test("file(dir, file)", proc()
      assert(assert.file("/my/dir", "file.txt")).is("FileWrapper").mem("_path").eq("/my/dir/file.txt")
    end)
  end)

  ############
  # exists() #
  ############
  suite("exists()", proc()
    test("exists()", proc()
      const f = assert.file("test/data/empty.txt")
      assert(f.exists()).sameAs(f)
    end)

    test("exists() - error, existing item but it not being file", proc()
      assert(proc() assert.file("test/data").exists() end).raises("file 'test/data' must exist.")
    end)

    test("exists() - error, not existing item", proc()
      assert(proc() assert.file("unknown").exists() end).raises("file 'unknown' must exist.")
    end)
  end)

  ###############
  # notExists() #
  ###############
  suite("notExists()", proc()
    test("doesNotExist()", proc()
      const f = assert.file("test/data")
      assert(f.doesNotExist()).sameAs(f)
    end)

    test("notExists() - item exists but not being file", proc()
      const f = assert.file("test/data")
      assert(f.notExists()).sameAs(f)
    end)

    test("notExists() - item existing", proc()
      assert(proc()
        assert.file("test/data/empty.txt").notExists()
      end).raises("file 'test/data/empty.txt' must not exist.")
    end)
  end)

  ##############
  # includes() #
  ##############
  suite("includes()", proc()
    const f = assert.file("test/data/file.txt")

    test("includes(text)", proc()
      assert(f.includes("hola")).sameAs(f)
    end)

    test("includes(text) - error", proc()
      assert(proc()
        f.includes("unknown")
      end).raises("file 'test/data/file.txt' must include 'unknown'.")
    end)

    test("includes(num)", proc()
      assert(f.includes(123)).sameAs(f)
    end)

    test("includes(num) - error", proc()
      assert(proc() f.includes(456) end).raises("file 'test/data/file.txt' must include '456'.")
    end)

    test("includes(bool)", proc()
      assert(f.includes(true)).sameAs(f)
    end)

    test("includes(bool) - error", proc()
      assert(proc() f.includes(false) end).raises("file 'test/data/file.txt' must include 'false'.")
    end)

    test("includes(list)", proc()
      assert(f.includes(["ciao", "hola"])).sameAs(f)
    end)

    test("includes(list) - error when none", proc()
      assert(proc()
        f.includes(["unknown", "none"])
      end).raises("file 'test/data/file.txt' must include 'unknown'.")
    end)

    test("includes(list) - error when someone", proc()
      assert(proc()
        f.includes(["hola", "unknown", "ciao"])
      end).raises("file 'test/data/file.txt' must include 'unknown'.")
    end)
  end)

  #################
  # notIncludes() #
  #################
  suite("notIncludes()", proc()
    const f = assert.file("test/data/file.txt")

    test("doesNotInclude(text)", proc()
      assert(f.doesNotInclude("unknown")).sameAs(f)
    end)

    test("notIncludes(text)", proc()
      assert(f.notIncludes("unknown")).sameAs(f)
    end)

    test("notIncludes(text) - error", proc()
      assert(proc()
        f.notIncludes("hello")
      end).raises("file 'test/data/file.txt' must not include 'hello'.")
    end)

    test("notIncludes(num)", proc()
      assert(f.notIncludes(456)).sameAs(f)
    end)

    test("notIncludes(num) - error", proc()
      assert(proc()
        f.notIncludes(123)
      end).raises("file 'test/data/file.txt' must not include '123'.")
    end)

    test("notIncludes(bool)", proc()
      assert(f.notIncludes(false)).sameAs(f)
    end)

    test("notIncludes(bool) - error", proc()
      assert(proc()
        f.notIncludes(true)
      end).raises("file 'test/data/file.txt' must not include 'true'.")
    end)

    test("notIncludes(list)", proc()
      assert(f.notIncludes(["unk", "nown"])).sameAs(f)
    end)

    test("notIncludes(list) - error when everyone", proc()
      assert(proc()
        f.notIncludes(["hello", "ciao"])
      end).raises("file 'test/data/file.txt' must not include 'hello'.")
    end)

    test("notIncludes(list) - error when someone", proc()
      assert(proc()
        f.notIncludes(["hola", "unknown", "ciao"])
      end).raises("file 'test/data/file.txt' must not include 'hola'.")
    end)
  end)

  #############
  # isEmpty() #
  #############
  suite("isEmpty()", proc()
    test("isEmpty()", proc()
      const f = assert.file("test/data/empty.txt")
      assert(f.isEmpty()).sameAs(f)
    end)

    test("isEmpty() - error", proc()
      assert(proc()
        assert.file("test/data/file.txt").isEmpty()
      end).raises("file 'test/data/file.txt' must be empty.")
    end)
  end)

  ################
  # isNotEmpty() #
  ################
  suite("isNotEmpty()", proc()
    test("isNotEmpty()", proc()
      const f = assert.file("test/data/file.txt")
      assert(f.isNotEmpty()).sameAs(f)
    end)

    test("isNotEmpty() - error", proc()
      assert(proc()
        assert.file("test/data/empty.txt").isNotEmpty()
      end).raises("file 'test/data/empty.txt' must not be empty.")
    end)
  end)

  ########
  # eq() #
  ########
  suite("eq()", proc()
    test("eq(text)", proc()
      const f = assert.file("test/data/eq.txt")
      assert(f.eq("ciao mondo!\n")).sameAs(f)
    end)

    test("eq(text) - error", proc()
      assert(proc()
        assert.file("test/data/eq.txt").eq("ciao mondo!")
      end).raises("'test/data/eq.txt' file content must be 'ciao mondo!'.")
    end)
  end)

  ########
  # ne() #
  ########
  suite("ne()", proc()
    test("ne(text)", proc()
      const f = assert.file("test/data/eq.txt")
      assert(f.neq("hola mundo!\n")).sameAs(f)
    end)

    test("ne(text) - error", proc()
      assert(proc()
        assert.file("test/data/eq.txt").ne("ciao mondo!\n")
      end).raises("'test/data/eq.txt' file content must be 'ciao mondo!\n'.")
    end)
  end)

  ############
  # sameAs() #
  ############
  suite("sameAs()", proc()
    const f = assert.file("test/data/sameAs1.txt")

    test("sameAs(text)", proc()
      assert(f.sameAs("test/data/sameAs2.txt")).sameAs(f)
    end)

    test("sameAs(text) - error", proc()
      assert(proc()
        f.sameAs("test/data/file.txt")
      end).raises("file 'test/data/sameAs1.txt' must be same as file 'test/data/file.txt'.")
    end)
  end)

  ###############
  # notSameAs() #
  ###############
  suite("notSameAs()", proc()
    const f = assert.file("test/data/sameAs1.txt")

    test("notSameAs(text)", proc()
      assert(f.notSameAs("test/data/file.txt")).sameAs(f)
    end)

    test("notSameAs(text) - error", proc()
      assert(proc()
        f.notSameAs("test/data/sameAs2.txt")
      end).raises("file 'test/data/sameAs1.txt' must not be same as file 'test/data/sameAs2.txt'.")
    end)
  end)

  ############
  # isJson() #
  ############
  suite("isJson()", proc()
    test("ok", proc()
      const f = assert.file("test/data/my.json")
      assert(f.isJson()).sameAs(f)
    end)

    test("error", proc()
      assert(proc()
        assert.file("test/data/empty.txt").isJson()
      end).raises("file 'test/data/empty.txt' must be JSON.")
    end)
  end)

  ################
  # startsWith() #
  ################
  suite("startsWith()", proc()
    test("ok", proc()
      const f = assert.file("test/data/file.txt")
      assert(f.startsWith("bon")).sameAs(f)
    end)

    test("error", proc()
      assert(proc()
        assert.file("test/data/file.txt").startsWith("123")
      end).raises("file 'test/data/file.txt' must start with '123'.")
    end)
  end)

  ###################
  # notStartsWith() #
  ###################
  suite("notStartsWith()", proc()
    test("ok", proc()
      const f = assert.file("test/data/file.txt")
      assert(f.notStartsWith("123")).sameAs(f)
    end)

    test("error", proc()
      assert(proc()
        assert.file("test/data/file.txt").doesNotStartWith("bon")
      end).raises("file 'test/data/file.txt' must not start with 'bon'.")
    end)
  end)

  ##############
  # endsWith() #
  ##############
  suite("endsWith()", proc()
    test("ok", proc()
      const f = assert.file("test/data/file.txt")
      assert(f.endsWith("123\n")).sameAs(f)
    end)

    test("error", proc()
      assert(proc()
        assert.file("test/data/file.txt").endsWith("321")
      end).raises("file 'test/data/file.txt' must end with '321'.")
    end)
  end)

  #################
  # notEndsWith() #
  #################
  suite("notEndsWith()", proc()
    test("ok", proc()
      const f = assert.file("test/data/file.txt")
      assert(f.notEndsWith("321")).sameAs(f)
    end)

    test("error", proc()
      assert(proc()
        assert.file("test/data/file.txt").doesNotEndWith("123\n")
      end).raises("file 'test/data/file.txt' must not end with '123\n'.")
    end)
  end)
end)
